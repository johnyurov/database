package com.example.lab5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    DBHelper dbHelper;
    TableLayout tableLayout;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DBHelper(this);
        tableLayout = (TableLayout) findViewById(R.id.tableLayout);
        textView = (TextView) findViewById(R.id.textView17);
    }

    public void onCreate(View view) {
        Random random = new Random(System.currentTimeMillis());
        String nameArr[] = {"Данил","Артур","Сергей","Женя","Таня","Оля","Вика","Маша","Кирилл","Вася","Наташа","Петя","Алексей","Ваня","Дарина","Александр","Ульяна"};
        int weight = random.nextInt(100 + 1) + 50;
        int height = random.nextInt(40 + 1) + 160;
        int age = random.nextInt(82 + 1) + 18;
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.KEY_NAME, nameArr[random.nextInt(15 - 0)]);
        contentValues.put(DBHelper.KEY_WEIGHT, String.valueOf(weight));
        contentValues.put(DBHelper.KEY_HEIGHT, String.valueOf(height));
        contentValues.put(DBHelper.KEY_AGE,  String.valueOf(age));
        database.insert(DBHelper.TABLE_CONTACTS, null, contentValues);
        textView.setText("successful");
    }
    public void onClick(View view) {

        SQLiteDatabase database = dbHelper.getWritableDatabase();
                Cursor cursor = database.query(DBHelper.TABLE_CONTACTS, null, null, null, null, null ,"AGE ASC");
                if (cursor.moveToFirst()) {
                    do {
                        int nameIndex = cursor.getColumnIndex(DBHelper.KEY_NAME);
                        int weightIndex = cursor.getColumnIndex(DBHelper.KEY_WEIGHT);
                        int heightIndex = cursor.getColumnIndex(DBHelper.KEY_HEIGHT);
                        int ageIndex = cursor.getColumnIndex(DBHelper.KEY_AGE);
                        TableRow tableRow = new TableRow(this);
                        TextView textView = new TextView(this);
                        textView.setText(cursor.getString(nameIndex));
                        tableRow.addView(textView);

                        TextView textView1 = new TextView(this);
                        textView1.setText(cursor.getString(weightIndex));
                        tableRow.addView(textView1);

                        TextView textView2 = new TextView(this);
                        textView2.setText(cursor.getString(heightIndex));
                        tableRow.addView(textView2);

                        TextView textView3 = new TextView(this);
                        textView3.setText(cursor.getString(ageIndex));
                        tableRow.addView(textView3);

                        tableLayout.addView(tableRow);
                   } while(cursor.moveToNext());
                }
//            break;
//        }
    }
}